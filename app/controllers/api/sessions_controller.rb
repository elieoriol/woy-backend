class Api::SessionsController < ApplicationController
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      render plain: user.access_token, status: 200
    else
      render plain: "Invalid email/password combination", status: 422
    end
  end

  def check_access_token
    user = User.find_by(access_token: params[:session][:access_token])
    if user
      render plain: "verified", status: 200
    else
      render plain: "Token failed verification", status: 422
    end
  end
end
