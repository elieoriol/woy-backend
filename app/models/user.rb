class User < ApplicationRecord
  before_save   :downcase_email
  before_create :generate_access_token

  validates :name,  presence: true, length: { maximum: 50 }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }

  has_secure_password
  validates :password, length: { minimum: 6 }, allow_nil: true

  # Digest hash of string
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine
    BCrypt::Password.create(string, cost: cost)
  end

  # Random token
  def User.new_token
    SecureRandom.uuid
  end

  # Authenticate a token
  def authenticated?(attribute, token)
    digest = self.send("#{attribute_digest}")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end


  private

    # Downcase email
    def downcase_email
      self.email = email.downcase
    end

    def generate_access_token
      begin
        self.access_token = User.new_token
      end while self.class.exists?(access_token: access_token)
    end
end
