class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users, id: :uuid, default: 'gen_random_uuid()'  do |t|
      t.string :name
      t.string :email
      t.string :password_digest
      t.uuid :access_token

      t.timestamps
    end
  end
end
