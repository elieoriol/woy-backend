require 'test_helper'

class Api::SessionsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get api_sessions_new_url
    assert_response :success
  end

  test "should get create" do
    get api_sessions_create_url
    assert_response :success
  end

  test "should get check_access_token" do
    get api_sessions_check_access_token_url
    assert_response :success
  end

end
